// Internal functions that the basemod and DLL can use to communicate

class Internal {
    foreign static tweaker_enabled=(value) // Disable the tweaker if the basemod is using the DB hook system instead
}
